package com.yami.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yami.shop.bean.app.param.OrderEntryParam;
import com.yami.shop.bean.model.OrderEntry;
import com.yami.shop.bean.model.TzUserInviteCode;
import com.yami.shop.dao.OrderEntryMapper;
import com.yami.shop.service.OrderEntryService;

import com.yami.shop.service.TzUserInviteCodeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Objects;

/**
* @author Administrator
* @description 针对表【tz_order_entry】的数据库操作Service实现
* @createDate 2024-02-29 20:27:14
*/
@Service
@Log4j2
public class OrderEntryServiceImpl extends ServiceImpl<OrderEntryMapper, OrderEntry>
    implements OrderEntryService{

    @Autowired
    private TzUserInviteCodeService tzUserInviteCodeService;
    @Override
    @Transactional
    public String orderEntry(String userId, OrderEntryParam orderNo) {
        if (Objects.isNull(orderNo)) {
            return "订单号不能为空！";
        }

        //根据现有的用户Id查到邀请码表
        LambdaQueryWrapper<TzUserInviteCode> orderEntryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        orderEntryLambdaQueryWrapper.eq(TzUserInviteCode::getInviteeUserId,userId);
        TzUserInviteCode tzUserInviteCode = tzUserInviteCodeService.getBaseMapper().selectOne(orderEntryLambdaQueryWrapper);

        if (Objects.isNull(tzUserInviteCode)) {
            return "没有找到对应的代理人信息,请输入代理人邀请码！";
        }

        LambdaQueryWrapper<OrderEntry> orderEntryLambdaQueryWrapper1 = new LambdaQueryWrapper<>();
        orderEntryLambdaQueryWrapper1.eq(OrderEntry::getOrderEntryUser,userId);

        OrderEntry orderEntry1 = baseMapper.selectOne(orderEntryLambdaQueryWrapper1);
        if (Objects.nonNull(orderEntry1)){
            return "不能重复录入";
        }

        //生成订单录入表
        OrderEntry orderEntry = new OrderEntry();
        orderEntry.setOrderNumber(orderNo.getOrderNumber());
        orderEntry.setOrderEntryUser(userId);
        orderEntry.setIsDelete(0L);
        orderEntry.setPaymentReceivingUser(tzUserInviteCode.getInviteUserId());
        orderEntry.setCreateTime(new Date());

        boolean save = this.save(orderEntry);
        if(!save){
            log.info("录入订单保存：{}",false);
            return "录入订单保存失败，请稍后重试！";
        }
        return "订单录入成功！";
    }
}




