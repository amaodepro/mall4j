package com.yami.shop.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yami.shop.bean.app.param.OrderEntryParam;
import com.yami.shop.bean.model.OrderEntry;

/**
* @author Administrator
* @description 针对表【tz_order_entry】的数据库操作Service
* @createDate 2024-02-29 20:27:14
*/
public interface OrderEntryService extends IService<OrderEntry> {

    String orderEntry(String userId, OrderEntryParam orderNo);
}
