package com.yami.shop.api.controller;

import com.yami.shop.bean.app.param.OrderEntryParam;
import com.yami.shop.bean.model.OrderEntry;
import com.yami.shop.common.response.ServerResponseEntity;
import com.yami.shop.security.api.model.YamiUser;
import com.yami.shop.security.api.util.SecurityUtils;
import com.yami.shop.service.OrderEntryService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/p/orderEntry")
@Tag(name = "订单录入接口")
public class OrderEntryController {
    @Autowired
    private OrderEntryService orderEntryService;

    @PostMapping("/orderNo")
    public ServerResponseEntity<String> orderEntry(@RequestBody OrderEntryParam orderNumber){
        YamiUser user = SecurityUtils.getUser();
        String userId = user.getUserId();
        String b = orderEntryService.orderEntry(userId,orderNumber);
        return  ServerResponseEntity.success(b);
    }

}
