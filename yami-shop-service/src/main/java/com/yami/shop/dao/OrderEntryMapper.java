package com.yami.shop.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yami.shop.bean.model.OrderEntry;

/**
* @author Administrator
* @description 针对表【tz_order_entry】的数据库操作Mapper
* @createDate 2024-02-29 20:27:14
* @Entity com.yami.shop.domain.OrderEntry
*/
public interface OrderEntryMapper extends BaseMapper<OrderEntry> {

}




