package com.yami.shop.bean.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName tz_order_entry
 */
@TableName(value ="tz_order_entry")
@Data
public class OrderEntry implements Serializable {
    /**
     * 订单录入主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long orderEntryId;

    /**
     * 录入订单
     */
    private String orderNumber;

    /**
     * 订单录入用户
     */
    private String orderEntryUser;

    /**
     * 录入用户电话号码
     */
    private String phoneNumber;

    /**
     * 收款用户主键
     */
    private String paymentReceivingUser;

    /**
     * 是否删除
     */
    private Long isDelete;

    /**
     * 创建时间
     */
    private Date createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        OrderEntry other = (OrderEntry) that;
        return (this.getOrderEntryId() == null ? other.getOrderEntryId() == null : this.getOrderEntryId().equals(other.getOrderEntryId()))
            && (this.getOrderNumber() == null ? other.getOrderNumber() == null : this.getOrderNumber().equals(other.getOrderNumber()))
            && (this.getOrderEntryUser() == null ? other.getOrderEntryUser() == null : this.getOrderEntryUser().equals(other.getOrderEntryUser()))
            && (this.getPhoneNumber() == null ? other.getPhoneNumber() == null : this.getPhoneNumber().equals(other.getPhoneNumber()))
            && (this.getPaymentReceivingUser() == null ? other.getPaymentReceivingUser() == null : this.getPaymentReceivingUser().equals(other.getPaymentReceivingUser()))
            && (this.getIsDelete() == null ? other.getIsDelete() == null : this.getIsDelete().equals(other.getIsDelete()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getOrderEntryId() == null) ? 0 : getOrderEntryId().hashCode());
        result = prime * result + ((getOrderNumber() == null) ? 0 : getOrderNumber().hashCode());
        result = prime * result + ((getOrderEntryUser() == null) ? 0 : getOrderEntryUser().hashCode());
        result = prime * result + ((getPhoneNumber() == null) ? 0 : getPhoneNumber().hashCode());
        result = prime * result + ((getPaymentReceivingUser() == null) ? 0 : getPaymentReceivingUser().hashCode());
        result = prime * result + ((getIsDelete() == null) ? 0 : getIsDelete().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", orderEntryId=").append(orderEntryId);
        sb.append(", orderNumber=").append(orderNumber);
        sb.append(", orderEntryUser=").append(orderEntryUser);
        sb.append(", phoneNumber=").append(phoneNumber);
        sb.append(", paymentReceivingUser=").append(paymentReceivingUser);
        sb.append(", isDelete=").append(isDelete);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}