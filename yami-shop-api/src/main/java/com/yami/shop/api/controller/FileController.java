/*
 * Copyright (c) 2018-2999 广州市蓝海创新科技有限公司 All rights reserved.
 *
 * https://www.mall4j.com/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.yami.shop.api.controller;

import com.yami.shop.api.utils.UserInfoUtils;
import com.yami.shop.common.bean.Qiniu;
import com.yami.shop.common.response.ResponseEnum;
import com.yami.shop.common.response.ServerResponseEntity;
import com.yami.shop.common.util.ImgUploadUtil;
import com.yami.shop.security.common.bo.UserInfoInTokenBO;
import com.yami.shop.security.common.manager.TokenStore;
import com.yami.shop.service.AttachFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * 文件上传 controller
 *
 * @author lgh
 */
@RestController
@RequestMapping("/user/file")
public class FileController {

    @Autowired
    private AttachFileService attachFileService;
    @Autowired
    private Qiniu qiniu;
    @Autowired
    private ImgUploadUtil imgUploadUtil;
    @Autowired
    private TokenStore tokenStore;

    /**
     * 获取上级代理图像
     * @return
     */
    @PostMapping("/getParentPaymentCode")
    public ResponseEntity<Resource> getParentPaymentCode(){
        UserInfoInTokenBO userInfo = UserInfoUtils.getUserInfo(tokenStore);
        if (Objects.isNull(userInfo)) {
            throw new RuntimeException("获取用户信息出错了~");
        }
        String userId = userInfo.getUserId();
        return attachFileService.getParentPaymentCode(userId);
    }

    @GetMapping("/getParment")
    public ResponseEntity<Resource> getImage() {
        UserInfoInTokenBO userInfo = UserInfoUtils.getUserInfo(tokenStore);
        if (Objects.isNull(userInfo)) {
            throw new RuntimeException("获取用户信息出错了~");
        }
        String userId = userInfo.getUserId();
       return attachFileService.getParment(userId);
    }

    @PostMapping("/upload/paymentCode")
    public ServerResponseEntity<String> uploadElementFile(@RequestParam("file") MultipartFile file) throws IOException {
        UserInfoInTokenBO userInfo = UserInfoUtils.getUserInfo(tokenStore);
        if (Objects.isNull(userInfo)) {
            throw new RuntimeException("获取用户信息出错了~");
        }
        String userId = userInfo.getUserId();

        if (file.isEmpty()) {
            return ServerResponseEntity.success();
        }
        String fileName = attachFileService.uploadFileToLocal(file, userId);
        return ServerResponseEntity.success(fileName);
    }

    @PostMapping("/upload/tinymceEditor")
    public ServerResponseEntity<String> uploadTinymceEditorImages(@RequestParam("editorFile") MultipartFile editorFile) throws IOException {
        String fileName = attachFileService.uploadFile(editorFile);
        String data = "";
        if (Objects.equals(imgUploadUtil.getUploadType(), 1)) {
            data = imgUploadUtil.getUploadPath() + fileName;
        } else if (Objects.equals(imgUploadUtil.getUploadType(), 2)) {
            data = qiniu.getResourcesUrl() + fileName;
        }
        return ServerResponseEntity.success(data);
    }

}
