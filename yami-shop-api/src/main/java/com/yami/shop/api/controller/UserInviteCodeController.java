package com.yami.shop.api.controller;

import com.yami.shop.common.response.ServerResponseEntity;
import com.yami.shop.security.api.util.SecurityUtils;
import com.yami.shop.service.TzUserInviteCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/p/invite")
public class UserInviteCodeController {
    @Autowired
    private TzUserInviteCodeService service;

    /**
     * 加入邀请
     * @param inviteCode
     * @return
     */
    @GetMapping("/joinInvite")
    public ServerResponseEntity<String> joinInvite(String inviteCode){
        String userId = SecurityUtils.getUser().getUserId();
        String isJoin = service.joinInvite(userId,inviteCode);
        if(isJoin == null){
            return ServerResponseEntity.success();
        }else{
            return ServerResponseEntity.showFailMsg(isJoin);
        }
    }
}
