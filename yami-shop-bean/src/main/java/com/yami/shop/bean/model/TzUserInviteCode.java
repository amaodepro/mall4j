package com.yami.shop.bean.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

import javax.annotation.processing.Generated;

/**
 * 
 * @TableName tz_user_invite_code
 */
@TableName(value ="tz_user_invite_code")
@Data
public class TzUserInviteCode implements Serializable {
    /**
     * 用户邀请码主键
     */
    @TableId( type = IdType.ASSIGN_ID)
    private Long tzUserInviteCodeId;

    /**
     * 邀请人主键
     */
    private String inviteUserId;

    /**
     * 被邀请人主键
     */
    private String inviteeUserId;

    /**
     * 邀请时间
     */
    private Date inviteTime;

    /**
     * 是否删除
     */
    private Integer isDelete;
    /**
     * 邀请码
     */
    private String inviteCode;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TzUserInviteCode other = (TzUserInviteCode) that;
        return (this.getTzUserInviteCodeId() == null ? other.getTzUserInviteCodeId() == null : this.getTzUserInviteCodeId().equals(other.getTzUserInviteCodeId()))
            && (this.getInviteUserId() == null ? other.getInviteUserId() == null : this.getInviteUserId().equals(other.getInviteUserId()))
            && (this.getInviteeUserId() == null ? other.getInviteeUserId() == null : this.getInviteeUserId().equals(other.getInviteeUserId()))
            && (this.getInviteTime() == null ? other.getInviteTime() == null : this.getInviteTime().equals(other.getInviteTime()))
            && (this.getIsDelete() == null ? other.getIsDelete() == null : this.getIsDelete().equals(other.getIsDelete()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getTzUserInviteCodeId() == null) ? 0 : getTzUserInviteCodeId().hashCode());
        result = prime * result + ((getInviteUserId() == null) ? 0 : getInviteUserId().hashCode());
        result = prime * result + ((getInviteeUserId() == null) ? 0 : getInviteeUserId().hashCode());
        result = prime * result + ((getInviteTime() == null) ? 0 : getInviteTime().hashCode());
        result = prime * result + ((getIsDelete() == null) ? 0 : getIsDelete().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", tzUserInviteCodeId=").append(tzUserInviteCodeId);
        sb.append(", inviteUserId=").append(inviteUserId);
        sb.append(", inviteeUserId=").append(inviteeUserId);
        sb.append(", inviteTime=").append(inviteTime);
        sb.append(", isDelete=").append(isDelete);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}