package com.yami.shop.bean.app.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class OrderEntryParam {
    /**
     * 订单号
     */
    @NotBlank(message="订单号不能为空")
    @Schema(description = "订单号" ,required=true)
    private String orderNumber;
}
