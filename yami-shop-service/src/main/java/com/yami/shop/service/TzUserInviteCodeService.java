package com.yami.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yami.shop.bean.model.TzUserInviteCode;

/**
 * @author Administrator
 * @description 针对表【tz_user_invite_code】的数据库操作Service
 * @createDate 2024-02-28 20:48:21
 */
public interface TzUserInviteCodeService extends IService<TzUserInviteCode> {
    /**
     * 加入邀请
     * @param userId
     * @param inviteCode
     * @return
     */
    String joinInvite(String userId, String inviteCode);
}