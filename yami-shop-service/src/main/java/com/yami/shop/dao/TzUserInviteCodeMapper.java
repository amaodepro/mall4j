package com.yami.shop.dao;

import com.yami.shop.bean.model.TzUserInviteCode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【tz_user_invite_code】的数据库操作Mapper
* @createDate 2024-02-28 20:48:21
* @Entity com.yami.shop.bean.model.TzUserInviteCode
*/
public interface TzUserInviteCodeMapper extends BaseMapper<TzUserInviteCode> {

}




